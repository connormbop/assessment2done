﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SupportForm_10000213
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public int toAddress = 0;
        public int subject = 0;
        public string fromAddress = "";
        public string message = "";

        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            int toAddress = toAddBox.SelectedIndex;
            string fromAddress = frmAddBox.Text;
            int subject = subBox.SelectedIndex;
            string message = mssgeBox.Text;

            string c = To(toAddress);
            string b = Subject(subject);

            MessageDialog text = new MessageDialog($"\n\t To: {c}\n\t From: {fromAddress}\n\t Subject Priority: {b} \n\n\t Message: {message}");
            text.Title = "Are you sure?";

            text.Commands.Add(new Windows.UI.Popups.UICommand($"Send to {c}") { Id = 0 });
            text.Commands.Add(new Windows.UI.Popups.UICommand("Cancel") { Id = 1 });
            text.Commands.Add(new Windows.UI.Popups.UICommand("Reset") { Id = 2 });

            text.DefaultCommandIndex = 0;
            text.CancelCommandIndex = 1;

            var output = await text.ShowAsync();

            if ((int)output.Id == 0)
            {
                Application.Current.Exit();

            }
            else if ((int)output.Id == 2)
            {
                toAddBox.SelectedIndex = -1;
                frmAddBox.Text = "";
                subBox.SelectedIndex = -1;
                mssgeBox.Text = "";
            }


        }
        static string To(int toAddress)
        {
            var d = "";

            switch (toAddress)
            {
                case 0:
                    d = "Microsoft@ms.co.nz";
                    break;
                case 1:
                    d = "Apple@Mac.com";
                    break;
                case 2:
                    d = "Hewlett@Packard.com";
                    break;
                case 3:
                    d = "Dell@Monitors.com";
                    break;
                case 4:
                    d = "Trade@Me.co.nz";
                    break;

            }
            return d;
        }
        static string Subject(int subject)
        {
            var a = "";

            switch (subject)
            {
                case 0:
                    a = "Low Priority";
                    break;
                case 1:
                    a = "Medium Priority";
                    break;
                case 2:
                    a = "High Priority";
                    break;
            }

            return a;
        }
    }
}

